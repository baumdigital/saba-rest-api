//Carga la configuración del sitio
var Config = {
    Constants: require('./config/Constants.js'),
    Database: require('./config/Database.js'),
    Email: require('./config/Email.js'),
    Notification: require('./config/Notification.js'),
    SDKs: require('./config/SDKs.js'),
    System: require('./config/System.js'),
    Storage: require('./config/Storage.js'),
    Lang: require('./config/Lang.js')
};
module.exports = Config;
