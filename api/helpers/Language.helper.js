//imports
var validator = require('validator'),
    Config = require('../../config.js'),
    LangConfig = Config.Lang;

/*
 *
 * @param {Mixed} req
 * @param {Mixed} res
 * @param {Mixed} next
 * @returns {Array}
 */
module.exports.setLanguage = function(req, res, next) {
    if (!req.headers['accept-language']) {
        global.lang = LangConfig.default;
    } else if (LangConfig.available.indexOf(req.headers['accept-language']) === -1) {
        global.lang = LangConfig.default;
    } else {
        global.lang = req.headers['accept-language'];
    }
    next();
};
