//imports
var ValidatorHelper = require('./Validator.helper.js'),
    SecurityHelper = require('./Security.helper.js'),
    CodigoController = require('../controllers/Codigo.controller.js');

module.exports.initModules = function(req, res, next) {
    ValidatorHelper.init();
    SecurityHelper.init();
    CodigoController.init();
    next();
}
