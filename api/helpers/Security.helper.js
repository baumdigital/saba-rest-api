var bcrypt = require('bcrypt-nodejs'),
    jwt = require('jsonwebtoken'),
    sequelize = require('../core/Sequelize.js'),
    Config = require('../../config.js');

var Errors, ErrorSystem, ErrorStrings;


//function init
module.exports.init = function() {
    Errors = Config.Lang.errors[global.lang];
    ErrorSystem = Errors.system;
    ErrorStrings = Errors.strings;
}

/* --------------------------------- password --------------------------------- */
module.exports.getHash = function(word) {
    var salt = bcrypt.genSaltSync();
    return bcrypt.hashSync(word, salt);
};

module.exports.compareHash = function(word, hashed) {
    return bcrypt.compareSync(word, hashed);
};

/* --------------------------------- cliente tokens --------------------------------- */
module.exports.createClienteToken = function(id, email, name, callBack) {
    var secret = id + '-cliente-' + new Date().getTime();
    var token = jwt.sign({
        id: id,
        email: email,
        name: name,
        tipo: 'cliente'
    }, secret);
    sequelize.query("INSERT INTO cm_session_token (token, secret, created_at, updated_at) VALUES ('" + token + "', '" + secret + "', NOW(), NOW())")
        .spread(function(results, metadata) {
            if (metadata.affectedRows === 0) {
                callBack('');
            } else {
                callBack(token);
            }
        });
};

module.exports.updateClienteToken = function(id, email, name, old_token, callBack) {
    var secret = id + '-cliente-' + new Date().getTime();
    var token = jwt.sign({
        id: id,
        email: email,
        name: name,
        tipo: 'cliente'
    }, secret);
    sequelize.query("UPDATE cm_session_token SET token='" + token + "', secret='" + secret + "', updated_at=NOW() WHERE token='" + old_token + "'")
        .spread(function(results, metadata) {
            if (metadata.affectedRows === 0) {
                callBack('');
            } else {
                callBack(token);
            }
        });
};

module.exports.verifyClienteToken = function(req, res, next) {
    if (!req.headers.authorization && !Config.System.debug) {
        res.status(401);
        res.json({
            valido: false,
            error: {
                mensaje: ErrorStrings.requires_authorization
            }
        });
        return;
    }
    if (!Config.System.debug) {
        sequelize.query("SELECT secret FROM cm_session_token WHERE token='" + req.headers.authorization + "' LIMIT 1", {
                type: sequelize.QueryTypes.SELECT
            })
            .then(function(result) {
                if (!result[0]) {
                    res.status(401);
                    res.json({
                        valido: false,
                        error: {
                            mensaje: ErrorStrings.authorization_failed
                        }
                    });
                    return;
                }
                jwt.verify(req.headers.authorization, result[0].secret, function(err, decoded) {
                    if (err) {
                        res.status(401);
                        res.json({
                            valido: false,
                            error: {
                                mensaje: ErrorStrings.authorization_failed
                            }
                        });
                        return;
                    }
                    if (decoded.tipo !== 'cliente') {
                        res.status(401);
                        res.json({
                            valido: false,
                            error: {
                                mensaje: ErrorStrings.client_not_identified
                            }
                        });
                        return;
                    }
                    req.data_token = decoded;
                    next();
                });
            });

    } else {
        next();
    }
};

/* --------------------------------- tokens --------------------------------- */

module.exports.deleteToken = function(token, callBack) {
    sequelize.query("DELETE FROM cm_session_token WHERE token='" + token + "'")
        .spread(function(results, metadata) {
            if (metadata.affectedRows === 0) {
                callBack(false);
            } else {
                callBack(true);
            }
        });
};
