//Variables
var Request = require('request-promise');

/*
 * Función para realizar request por método get
 * @param {String} url
 * @param {Object-Not require} params
 * @param {Object-Not require} headers
 * @returns {Request} -> .then(function(response){}).catch(function(error){})
 */
module.exports.get = function (url, params, headers) {
    var options = {method: 'GET', uri: url, json: true, resolveWithFullResponse: true};
    if (params) {
        options.qs = params;
    }
    if (headers) {
        options.headers = headers;
    }
    return Request(options);
};

/*
 * Función para realizar request por método post
 * @param {String} url
 * @param {Object-Not require} params
 * @param {Object-Not require} headers
 * @returns {Request} -> .then(function(response){}).catch(function(error){})
 */
module.exports.post = function (url, params, headers) {
    var options = {method: 'POST', uri: url, json: true, resolveWithFullResponse: true};
    if (params) {
        options.body = params;
    }
    if (headers) {
        options.headers = headers;
    }
    return Request(options);
};

/*
 * Función para realizar request por método put
 * @param {String} url
 * @param {Object-Not require} params
 * @param {Object-Not require} headers
 * @returns {Request} -> .then(function(response){}).catch(function(error){})
 */
module.exports.put = function (url, params, headers) {
    var options = {method: 'PUT', uri: url, json: true, resolveWithFullResponse: true};
    if (params) {
        options.body = params;
    }
    if (headers) {
        options.headers = headers;
    }
    return Request(options);
};

/*
 * Función para realizar request por método delete
 * @param {String} url
 * @param {Object-Not require} params
 * @param {Object-Not require} headers
 * @returns {Request} -> .then(function(response){}).catch(function(error){})
 */
module.exports.delete = function (url, params, headers) {
    var options = {method: 'DELETE', uri: url, json: true, resolveWithFullResponse: true};
    if (params) {
        options.qs = params;
    }
    if (headers) {
        options.headers = headers;
    }
    return Request(options);
};