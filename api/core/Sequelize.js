var Sequelize = require('sequelize'),
    Config = require('../../config.js'),
    ConfigDatabase = Config.Database;

var sequelize = new Sequelize(ConfigDatabase.database, ConfigDatabase.username, ConfigDatabase.password, {
    dialect: 'mysql',
    timezone: Config.System.timezone_sequelize,
    port: ConfigDatabase.port,
    replication: {
        write: {
            host: ConfigDatabase.replicas.write.host,
            pool: {
                maxConnections: 20,
                maxIdleTime: 30000
            }
        },
        read: [{
            host: ConfigDatabase.replicas.read.host,
            pool: {
                maxConnections: 20,
                maxIdleTime: 30000
            }
        }]
    }
});
module.exports = sequelize;
