/* Routes de Clientes */

var express = require('express'),
    AppRouter = express.Router(),
    WebserviceRouter = express.Router(),
    SecurityHelper = require('../helpers/Security.helper.js'),
    ctrlCodigo = require('../controllers/Codigo.controller.js');

/**
 * @swagger
 * codigo/verificar:
 *   get:
 *     tags:
 *       - Codigos
 *     summary: Verifica la validez de un codigo
 *     description: Verifica  la validez de un codigo.
 *     operationId: verificar_codigo
 *     parameters:
 *      - name: country
 *        in: query
 *        description: Nombre del pais. Ej. Costa Rica
 *        required: true
 *        type: string
 *        format: string
 *      - name: code
 *        in: query
 *        description: codig. Ej. 1531211
 *        required: true
 *        type: string
 *        format: string
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Consulta exitosa
 *         schema:
 *           $ref: '#/definitions/VerificarCodigoResponse'
 *       400:
 *         description: Error en los parámetros o error al generar token de seguridad
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       409:
 *         description: Cuenta existente
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *
 */
AppRouter.get('/verificar', ctrlCodigo.AppVerificarCodigo);

/**
 * @swagger
 * codigo/activar:
 *   post:
 *     tags:
 *       - Codigos
 *     summary: Guarda un registro en la tabla ‘activations’
 *     description: Guarda un registro en la tabla ‘activations’
 *     operationId: activar_codigo
 *     parameters:
 *      - name: code
 *        in: formData
 *        description: codigo. Ej. 650
 *        required: true
 *        type: integer
 *        format: int64
 *      - name: country
 *        in: formData
 *        description: pais. Ej. Costa Rica
 *        required: true
 *        type: string
 *        format: string
 *      - name: phone_number
 *        in: formData
 *        description: numero de telefono. Ej. 1
 *        required: true
 *        type: string
 *        format: string
 *      - name: age
 *        in: formData
 *        description: edad. Ej. 1
 *        required: true
 *        type: integer
 *        format: int32
 *      - name: type
 *        in: formData
 *        description: tipo de codigo. Ej. 1
 *        required: true
 *        type: integer
 *        format: int32
 *      - name: identification
 *        in: formData
 *        description: identification. Ej. 1
 *        required: true
 *        type: string
 *        format: string
 *      - name: fecha
 *        in: formData
 *        description: fecha. Ej. 1
 *        required: true
 *        type: string
 *        format: dateonly
 *      - name: producto
 *        in: formData
 *        description: id del producto. Ej. 1
 *        required: true
 *        type: integer
 *        format: int32
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Cambio exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       400:
 *         description: Error en los parámetros o error al insertar
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       409:
 *         description: Cuenta existente
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *
 */
AppRouter.post('/activar', ctrlCodigo.AppActivarCodigo);

/**
 * @swagger
 * codigo/cambiar-estado:
 *   post:
 *     tags:
 *       - Codigos
 *     summary: Cambia el estado de un codigo
 *     description: Cambia el estado de un codigo
 *     operationId: verificar_codigo
 *     parameters:
 *      - name: id
 *        in: formData
 *        description: id del codigo. Ej. 650
 *        required: true
 *        type: integer
 *        format: int64
 *      - name: status
 *        in: formData
 *        description: numero del estado. Ej. 1
 *        required: true
 *        type: integer
 *        format: int32
 *     security:
 *      - Token: []
 *     responses:
 *       200:
 *         description: Cambio exitoso
 *         schema:
 *           $ref: '#/definitions/ValidoResponse'
 *       204:
 *         description: No hubo cambio en el codigo
 *         schema:
 *           $ref: '#/definitions/ValidoConMensajeResponse'
 *       400:
 *         description: Error en los parámetros o error al generar token de seguridad
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       409:
 *         description: Cuenta existente
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *       500:
 *         description: Error inesperado
 *         schema:
 *           $ref: '#/definitions/ErrorResponse'
 *
 */
AppRouter.post('/cambiar-estado', ctrlCodigo.AppCambiarEstadoCodigo);




module.exports.App = AppRouter;
