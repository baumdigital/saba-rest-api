//Variables
var SequelizeLib = require('sequelize'),
    sequelize = require('../core/Sequelize.js'),
     ValidatorHelper = require('../helpers/Validator.helper.js'),
    Config = require('../../config.js');



var Errors, ErrorSystem, ErrorStrings;


//function init
module.exports.init = function() {
    Errors = Config.Lang.errors[global.lang];
    ErrorSystem = Errors.system;
    ErrorStrings = Errors.strings;
}

// ============================================================================================================= //
// ============================================================================================================= //
// ============================================= Funciones del APP ============================================= //
// ============================================================================================================= //
// ============================================================================================================= //


module.exports.AppVerificarCodigo = (req, res) => {
    var validate = ValidatorHelper.validate({
        country: ['not_empty', ],
        code: ['not_empty','number']
    }, req.query);
    if (!validate.valido) {
        res.status(400);
        res.statusMessage = ErrorSystem["400"];
        res.json({
            valido: false,
            error: validate.error
        });
        return;
    }

    let country = req.query.country,
        code = req.query.code;

    console.log("======> country" + country);
    console.log("======> code" + code);
//codigo: code,

    return sequelize.query("SELECT c.id, c.status FROM (SELECT tmp.id, tmp.code, tmp.status FROM codes AS tmp WHERE tmp.country= :pais ) AS c WHERE c.code = " + code +" LIMIT 1; ", { replacements: {  pais: country }, type: sequelize.QueryTypes.SELECT })
        .then(codigos => {
            if (codigos.length == 0 || !codigos[0]) {
                res.status(404);
                res.statusMessage = ErrorSystem["409"];
                res.json({
                    valido: false,
                    error: {
                        mensaje: "código inválido"
                    }
                });
                return
;            }
            let codigo = codigos[0];
            res.status(200);
            res.statusMessage = 'OK';
            res.json({
                valido: true,
                data: {
                    id: codigo.id,
                    estado: codigo.status
                }
            })
            return;
        }).catch(function(error) {
            console.log(error);
            if (res.headersSent) return;
            res.statusMessage = ErrorSystem["500"];
            res.status(500);
            res.json({
                valido: false,
                error: {
                    mensaje: ErrorSystem["500"],
                    info: error
                }
            });
            return;
        });
}


/*


Guarda un registro en la tabla ‘activations






*/
module.exports.AppActivarCodigo = (req, res) => {
    var validate = ValidatorHelper.validate({
        code: ['not_empty'],
        country: ['not_empty'],
        phone_number: ['not_empty'],
        age: ['not_empty'],
        type: ['not_empty'],
        identification: ['not_empty'],
        fecha: ['not_empty'],
        producto: ['not_empty']
    }, req.body);
    if (!validate.valido) {
        res.status(400);
        res.statusMessage = ErrorSystem["400"];
        res.json({
            valido: false,
            error: validate.error
        });
        return;
    }

    let code = req.body.code,
        country = req.body.country,
        phone_number = req.body.phone_number,
        age = req.body.age,
        type = req.body.type,
        identification = req.body.identification,
        fecha = req.body.fecha,
        producto = req.body.producto;

    return sequelize.query("INSERT INTO activations (code,country,phone_number,age,type,identification,fecha,producto) values (?,?,?,?,?,?,?,?)", { replacements: [code,country,phone_number,age,type,identification,fecha,producto], type: sequelize.QueryTypes.INSERT })
          .then(data => {
            if (!data) {
                res.status(400);
                res.statusMessage = ErrorSystem["409"];
                res.json({
                    valido: false,
                    error: {
                        mensaje: "No se pudó realizar el insert"
                    }
                });
                return;
            }
            res.status(200);
            res.statusMessage = 'OK';
            res.json({
                valido: true
            })
            return;
        }).catch(function(error) {
            console.log(error);
            if (res.headersSent) return;
            res.statusMessage = ErrorSystem["500"];
            res.status(500);
            res.json({
                valido: false,
                error: {
                    mensaje: ErrorSystem["500"],
                    info: error
                }
            });
            return;
        });
}



module.exports.AppCambiarEstadoCodigo = (req, res) => {
    var validate = ValidatorHelper.validate({
        id: ['not_empty', 'number'],
        status: ['not_empty', 'number']
    }, req.body);
    if (!validate.valido) {
        res.status(400);
        res.statusMessage = ErrorSystem["400"];
        res.json({
            valido: false,
            error: validate.error
        });
        return;
    }

    let id = req.body.id,
        status = req.body.status;

    let update = ` UPDATE codes SET status='${status}' WHERE id='${id}' `;

    return sequelize.query(update)
        .spread( (result, metadata)  => {
            res.status(200);
            res.statusMessage = 'OK';
            res.json({
                valido: ((result.affectedRows == 0)? false : true),
                mensaje:  ((result.affectedRows == 0)? "No se pudó actualizar el estado de este código" : "Update exitoso")
            })
            return;
        }).catch(function(error) {
            console.log(error);
            if (res.headersSent) return;
            res.statusMessage = ErrorSystem["500"];
            res.status(500);
            res.json({
                valido: false,
                error: {
                    mensaje: ErrorSystem["500"],
                    info: error
                }
            });
            return;
        });
}