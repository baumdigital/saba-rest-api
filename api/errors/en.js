/**
 * List of errors
 *
 * Example: A-0001
 */
 module.exports = {
    strings: {
        required: "Required parameter",
        params_error: "Wrong parameters, check your query",
        empty: "This parameter cannot be empty",
        email: "This parameter must be a valid email",
        numbers: "This parameter must be an integer number",
        decimal: "This parameter must be a decimal number",
        boolean: "This parameter must be true or false",
        date: "This parameter must be a date and time value (YYYY-mm-dd HH:mm:ss)",
        date_format: "The required format for the date and time is (YYYY-mm-dd HH:mm:ss)",
        date_only: "This parameter must be a date (YYYY-mm-dd)",
        date_only_format: "The required format for the date is (YYYY-mm-dd)",
        greater_than_zero: "This parameter must be greater than zero",
        requires_authorization: "Requires authorization",
        authorization_failed: "Authorization failed",
        client_not_identified: "Client not identified",
        user_not_authorized: "Client not authorized to make this request"
    },
    system: {
        304: "Resource not modified",
        500: "Unexpected error",
        400: "Invalid parameters",
        401: "Authorization is required",
        402: "Payment is required",
        404: "Resource not found",
        409: "Inactive resource or there is a conflict with the query"
    },
    codes: {
        Cliente: {
            "0001": "There is a user already registered with the field identification",
            "0002": "This facebook id is already registered",
            "0003": "This email is already registered",
            "0004": "This user is not registered",
            "0005": "Incorrect password",
            "0006": "Inactive user",
            "0007": "There was an error trying to create the token, please try again",
            "0008": "User not found after being saved, try to login"
        },
        Notification: {
            "0001": "You must specify a valid platform Android o iOS"
        }
    }
};
