/**
 * Lista de los errores de la aplicación
 * Se establece una numeración para los errores
 * por controlador iniciando por las siglas que representan
 * al controlador y una numeración de #### digitos numericos
 * Ejemplo: A-0001
 */
 module.exports = {
    strings: {
        required: "Parametro requerido",
        params_error: "Parametros incorrectos o no válidos, revise su consulta",
        empty: "Este valor no puede estar vacío",
        email: "Este valor debe ser un email válido",
        numbers: "Este valor debe ser sólo números",
        decimal: "Este valor debe ser un número válido",
        boolean: "Valor no válido",
        date: "Este valor debe ser una fecha válida (YYYY-mm-dd HH:mm:ss)",
        date_format: "Este valor debe ser una fecha válida (YYYY-mm-dd HH:mm:ss)",
        date_only: "Este valor debe ser una fecha válida (YYYY-mm-dd)",
        date_only_format: "Este valor debe ser una fecha válida (YYYY-mm-dd)",
        greater_than_zero: "Este valor debe ser mayor a cero",
        requires_authorization: "Requiere autorización",
        authorization_failed: "Fallo de autorización",
        client_not_identified: "Cliente no identificado",
        user_not_authorized: "El usuario no tiene permisos para realizar este request"
    },
    system: {
        304: "El recurso no fue modificado",
        500: "Error inesperado",
        400: "Parámetros inválidos o con el formato incorrecto",
        401: "Autorización requerida o incorrecta",
        402: "Pago requerido",
        404: "Recurso no encontrado",
        409: "Recurso inactivo o existe un conflicto en la consulta"
    },
    codes: {
        Cliente: {
            "0001": "Esta identificación ya se encuentra registrada",
            "0002": "Este ID de Facebook ya se encuentra registrado",
            "0003": "Este email ya se encuentra registrado",
            "0004": "Cliente no registrado",
            "0005": "Contraseña incorrecta",
            "0006": "Cliente inactivo",
            "0007": "Ocurrió un error en la creación del token, por favor inténtelo de nuevo",
            "0008": "No se encontró el cliente después de guardar, intente loguear"
        },
        Notification: {
            "0001": "Debe especificar una plataforma válida Android o iOS"
        }
    }
};
