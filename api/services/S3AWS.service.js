//Variables
var Config = require('../../config.js'),
    ConfigAWSSDK = Config.SDKs.sdk_aws_config,
    S3AWSConfig = Config.Storage.s3_aws_config,
    AWS = require('aws-sdk');
//Configuracion
AWS.config.update(ConfigAWSSDK);
var s3Bucket = new AWS.S3({
    params: {
        Bucket: S3AWSConfig.bucket,
        ACL: 'public-read'
    }
});

/**
 * [saveImage description]
 * @param  {[type]} img         [description]
 * @param  {[type]} name        [description]
 * @param  {[type]} contentType [description]
 * @return {[type]}             [description]
 */
module.exports.saveImage = function(img, name, contentType, callBack) {
    var buf = new Buffer(img, 'base64')
    var data = {
        Key: name,
        Body: buf,
        ContentEncoding: 'base64',
        ContentType: contentType
    };
    s3Bucket.putObject(data, function(err, data) {
        if (err) {
            console.log(err);
            console.log('Error uploading data: ', data);
            return;
        } else {
            console.log('succesfully uploaded the image!');
            callBack();
            return;
        }
    });
};

/**
 * [deleteImage description]
 * @param  {[type]} file_dir [description]
 * @return {[type]}          [description]
 */
module.exports.deleteFile = function(file) {
    var params = {
        Key: file
    };
    s3Bucket.deleteObject(params, function(err, data) {
        if (data) {
            console.log("File deleted successfully");
        } else {
            console.log("Check if you have sufficient permissions : " + err);
        }
    });
};
