//Variables
var Config = require('../../config.js'),
    ConfigAWSSDK = Config.SDKs.sdk_aws_config,
    ConfigAWSSNS = Config.Notification.sns_aws_config,
    AWS = require('aws-sdk');

//Configuracion
AWS.config.update(ConfigAWSSDK);
var SNS = new AWS.SNS({
    apiVersion: ConfigAWSSNS.version
});

/*
 * Función para crear un endpoint en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.createEndpoint = function(deviceId, platform, lang, callBack) {
    var params = {
        Token: deviceId
    };
    var ARNConfig = ConfigAWSSNS.arns[lang];
    if (platform === 'iOS') {
        params.PlatformApplicationArn = ARNConfig.ios_arn;
    } else if (platform === 'Android') {
        params.PlatformApplicationArn = ARNConfig.android_arn;
    }
    SNS.createPlatformEndpoint(params, function(error, data) {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para suscribir un endpoint al topic en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.subscribeEndpoint = function(endpoint, lang, callBack) {
    var ARNConfig = ConfigAWSSNS.arns[lang];
    var params = {
        Protocol: 'application',
        TopicArn: ARNConfig.topic_arn,
        Endpoint: endpoint
    };
    SNS.subscribe(params, function(error, data) {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para suscribir un endpoint al topic de invitados en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.subscribeEndpointGuest = function(endpoint, lang, callBack) {
    var ARNConfig = ConfigAWSSNS.arns[lang];
    var params = {
        Protocol: 'application',
        TopicArn: ARNConfig.guest_topic_arn,
        Endpoint: endpoint
    };
    SNS.subscribe(params, function(error, data) {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para desvincularse de un topic en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.unsubscribeEndpoint = function(subscriptionArn, callBack) {
    var ARNConfig = ConfigAWSSNS.arns[lang];
    var params = {
        SubscriptionArn: subscriptionArn
    };
    SNS.unsubscribe(params, function(error, data) {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

/*
 * Función para realizar un push en SNS AWS
 * parametros:
 *  deviceId-> STRING
 *  platform-> STRING
 *  lang-> STRING
 *  callBack-> FUNCTION
 */
module.exports.publish = function(params, callBack) {
    SNS.publish(params, function(error, data) {
        if (error) {
            callBack(null, error);
        } else {
            callBack(data);
        }
    });
};

