//Variables
var Config = require('../../config.js'),
        MandrillConfig = Config.Email.mandrill_config,
        Mandrill = require('mandrill-api/mandrill'),
        MandrillClient = new Mandrill.Mandrill(MandrillConfig.production_key); //testing_key

/*
 * Función para realizar envíos por correo
 * parametros:
 *  to-> Array of objects: [{email: ''{, name: ''}}, {email: ''{, name: ''}}, ...]
 *  subject-> Text
 *  body-> HTML Text
 *  from-> Object: {email: ''{, name: ''}}
 */
module.exports.sendMail = function (to, subject, body, from, callBack) {
    if (!from) {
        from = MandrillConfig.default_from;
    }

    var message_params = {
        html: body,
        subject: subject,
        from_email: from.email,
        from_name: from.name,
        to: addTypeToEmailObjects(to),
        preserve_recipients: false
    };

    MandrillClient.messages.send({message: message_params}, function (result) {
        callBack(result);
    }, function (error) {
        callBack(null, error, message_params);
    });
};

function addTypeToEmailObjects(objects) {
    objects.forEach(function (object) {
        object.type = 'to';
    });
    return objects;
}
