//llamamos al server
var Server = require('./server');

/**
 * Escuchando en el puerto
 */
Server.app.listen(Server.Constants.port, function() {
    console.log('App running on port ' + Server.Constants.port);
});
