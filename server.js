/**
 * Las variables de entorno
 */

var path = require('path');

var env_path = path.resolve(__dirname, '.env');

function initEnv() {
    if (!process.env.NODE_ENV) {
        process.env = {};
    }
    require('dotenv').config({ path: env_path });
}
initEnv();
/**
 * Express
 */
var express = require('express'),
    app = express();
/**
 * Paquetes de terceros
 */
var compression = require('compression'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    path = require('path');
/**
 * Configuración del app *
 */
var Config = require('./config.js'),
    Constants = Config.Constants,
    LangHelper = require('./api/helpers/Language.helper.js');
/**
 * Logger de middleware
 */
app.use(logger('short'));
/**
 * Limite de tamaño de upload
 * @type {String}
 */
app.use(bodyParser.json({
    limit: '10000mb'
}));
/**
 * Límite de tamaño del request
 * @type {String}
 */
app.use(bodyParser.urlencoded({
    limit: '10000mb',
    extended: true
}));
/**
 * CORS
 */
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, OPTIONS,PUT,DELETE,POST');
    next();
});
/**
 * Set Language to default value
 */
global.lang = Config.Lang.default;
/**
 * Set Language to request value
 */
app.use(LangHelper.setLanguage);
/**
 * Call Init functions of modules
 */
var ModuleHelper = require('./api/helpers/Module.helper.js');
app.use(ModuleHelper.initModules);
/**
 * Swagger documentación del RESTful Api
 */
var swaggerJSDoc = require('swagger-jsdoc'),
    swaggerDefinition = require(path.join(__dirname, 'api/swagger/swagger.js'));
/**
 * GZip compression activada
 */
app.use(compression());
/**
 * Configuración de Swagger
 * @type {Object}
 */
var options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: [path.join(__dirname, 'api/routes/*.js')],
};
/**
 * Documentación con Swagger
 *
 */
var swaggerSpec = swaggerJSDoc(options);

/**
 * Documento de configuración de Swagger
 */

 
app.get('/json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});


/**
 * HTML de documentación
 */
app.use('/docs', express.static(path.join(__dirname, 'docs')));
/**
 * Routes archivos estaticos
 */
/*app.use('/files/email-templates', express.static('files/email-templates/'));
app.use('/files', function(req, res) {
    res.status(404);
    res.send('The resource "' + req.url + '" does no exist...');
});*/
app.use('/show', function(req, res) {
    var Email = require('./api/helpers/Email.helper.js');
    Email.Build(req.query.template, function(tpl) {
        res.send(tpl);
    });
});
/**
 * Routes
 */
var codigoRoute = require('./api/routes/Codigo.route.js');
/**
 * Api de los clientes
 * / producción
 * /test desarrollo o testing
 */
app.use('/codigo', codigoRoute.App);
// api de contenidos


/**
 * Error 404 si no encuentra ningún servicio
 */
app.use(function(req, res) {
    res.status(404);
    res.send('The resource URL: "' + req.url + '" AND Method: "' + req.method + '" does no exist...');
});

/**
 * Exportamos
 *
 */
module.exports.app = app;
module.exports.Constants = Constants;