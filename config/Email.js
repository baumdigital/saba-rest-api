//las constantes
var Constants = require('../config/Constants.js');
/**
 *Configuración de Email
 */
module.exports = {
    current_email_service: 'ses_aws', //ses_aws, mandrill o smtp
    mandrill_config: {
        default_from: {
            email: 'envios@baumtesting.com',
            name: 'PremFlight'
        },
        testing_key: '',
        production_key: ''
    },
    ses_aws_config: {
        version: '2010-12-01 ',
        default_from: {
            email: 'envios@baumsocialmedia.com',
            name: 'Starter'
        }
    },
    smtp_config: {
        default_from: {
            email: '',
            name: 'Starter'
        },
        settings: {
            host: 'smtp.gmail.com',
            port: 465,
            username: '',
            password: ''
        }
    },
    template_aliases: {
        '::img_logo::': 'http://api.premflight.com/static/logo.png',
        '::img_facebook::': 'http://api.premflight.com/static/color-facebook-48.png',
        '::img_twitter::': 'http://api.premflight.com/static/color-twitter-48.png',
        '::img_youtube::': 'http://api.premflight.com/static/color-youtube-48.png',
        '::img_instagram::': 'http://api.premflight.com/static/color-instagram-48.png',
        '::img_pinterest::': 'http://api.premflight.com/static/color-pinterest-48.png',
        '::img_googleplay::': 'http://api.premflight.com/static/googleplay.gif',
        '::img_appstore::': 'http://api.premflight.com/static/appstore.gif',
        '::url_facebook::': '#',
        '::url_twitter::': '#',
        '::url_youtube::': '#',
        '::url_instagram::': '#',
        '::url_googleplay::': 'https://play.google.com/store?hl=es',
        '::url_appstore::': 'https://www.apple.com/',
        '::current_year::': new Date().getFullYear()
    }
};
