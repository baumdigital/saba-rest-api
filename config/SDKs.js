/**
 *Configuración de los SDKs
 */
var mode = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';

module.exports = {
    sdk_aws_config: {
        accessKeyId: process.env.AWS_SDK_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SDK_SECRET_ACCESS_KEY,
        region: process.env.AWS_SDK_REGION
    }
};
