/**
 *Configuración de las notificaciones
 */
module.exports = {
    sns_aws_config: {
        version: '2010-03-31 ',
        arns: {
            es: {
                topic_arn: process.env.ES_AWS_TOPIC_ARN,
                guest_topic_arn: process.env.ES_AWS_GUEST_TOPIC_ARN,
                ios_arn: process.env.ES_AWS_IOS_ARN,
                android_arn: process.env.ES_AWS_ANDROID_ARN
            },
            en: {
                topic_arn: process.env.EN_AWS_TOPIC_ARN,
                guest_topic_arn: process.env.EN_AWS_GUEST_TOPIC_ARN,
                ios_arn: process.env.EN_AWS_IOS_ARN,
                android_arn: process.env.EN_AWS_ANDROID_ARN
            }
        }
    }
};
