/**
 *Configuración de almacenamiento
 */
module.exports = {	
    s3_aws_config: {
		url : 'https://s3.amazonaws.com/bucket-baum-loyalty/',
        version: '2006-03-01',
        bucket: 'bucket-baum-loyalty',
    }
};
