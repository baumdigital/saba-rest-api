/**
 *Configuración general del sistema
 */
module.exports = {
    default: 'es',
    available: ['es', 'en'],
    langId: { es: 1, en: 2 },
    errors: {
        es: require('../api/errors/es.js'),
        en: require('../api/errors/en.js')
    },
    email: {
        es: {
            bienvenida: {
                subject: 'Bienvenido a Starter',
                template: 'bienvenida-es'
            },
            bienvenidaFacebook: {
                subject: 'Bienvenido a Starter',
                template: 'bienvenida-facebook-es'
            },
            cambiarPassword: {
                subject: 'Solicitud de Cambio de Contraseña - Starter',
                template: 'servicio-al-cliente-es'
            },
            servicioAlCliente: {
                asunto: 'Servicio al cliente',
                template: 'servicio-al-cliente-es'
            }
        },
        en: {
            bienvenida: {
                subject: 'Welcome to Starter',
                template: 'bienvenida-en'
            },
            bienvenidaFacebook: {
                subject: 'Welcome to Starter',
                template: 'bienvenida-facebook-en'
            },
            cambiarPassword: {
                subject: 'Password Change Request - Starter',
                template: 'servicio-al-cliente-en'
            },
            servicioAlCliente: {
                subject: 'Customer Service',
                template: 'servicio-al-cliente-es'
            }
        }
    }
};
