/**
 *Configuración de la base de datos
 */
module.exports = {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    port: process.env.DB_PORT,
    replicas: {
        read: {
            host: process.env.DB_READ_HOST
        },
        write: {
            host: process.env.DB_WRITE_HOST
        }
    }
};